TARBALL=$1
shift
SPECNAME=$1
shift
MARKER=$1
shift
LOCALVERSION=$1
shift

SCRIPTS=scripts
SOURCES=rpmbuild/SOURCES
SRPMDIR=rpmbuild/SRPM
SPEC=rpmbuild/SPECS/${SPECNAME}

# Pre-cleaning
rm -rf psection

cp ${TARBALL} ${SOURCES}/${TARBALL}
cp ${SPECNAME} ${SPEC}


# Remove the old patch section
sed -i '/^Patch[0-9]*:/d' "${SPEC}"

num=0
for patchfile in ${SOURCES}/*.patch; do
  ignore=$(grep -x "Ignore-patch: .*" "$patchfile" | sed 's/Ignore-patch: \(.*\)/\1/')
  [ "$ignore" == "True" ] && rm "$patchfile" && continue
  patchname=$(basename "$patchfile")
  let num=num+1
  echo "Patch${num}: ${patchname}" >> psection
done

# Find the last line of the source section
lp=$(grep -e "Source[0-9]\+:.*" ${SPEC} | tail -n 1)

# Add the new patch section after the source section
sed -i "/$lp/r psection" ${SPEC}

# Append the local version (if any) to the release number
if [ -n "${LOCALVERSION}" ]; then
  sed -i "s/\(^Release:.*\)/\1\.${LOCALVERSION}/" ${SPEC}
fi

# Post-cleaning
rm -rf psection
